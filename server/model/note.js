const mongoose = require("mongoose");

const NoteSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    creationdate: {
        type: {},
        required: true
    },
    content: {
        type: String,
        required: true
    }
});

const Note = mongoose.model("Notes", NoteSchema);
module.exports = Note;